# Spinwheel Decider

## Features

This is a personal app, derived from that age old couples debate: What to have for dinner?

Work in progress, at the moment there is little functionality. The plan is to make a spin wheel
(almost like a roulette wheel) that can be filled with as many options as required and then decide
in a classic game show manner.

The plan is to use no pre built graphics, and have the circle segments shown using CSS only. Stay
tuned for more.

## Technologies Used

Languages used: React.js, JS, CSS, HTML

IDE: Notepad++
