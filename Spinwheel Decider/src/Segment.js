import React from 'react';
import './Segment.css';

class Segment extends React.Component {	
  render() {	  
	return (
	  <div className="Segment-holder" style={{transform: "translate(-50%, -50%) rotate(" + this.props.rotateDegree + "deg)"}}>
		<div className="Segment-outer" style={{transform: "skewX(" + this.props.skewDegrees + "deg)"}}>
		  <div className="Segment-inner" style={{transform: "skewX(-" + this.props.skewDegrees + "deg)", backgroundColor: '#'+Math.floor(Math.random()*16777215).toString(16)}}>
		    <div className="Segment-text">
			  {this.props.choiceText}
	        </div>
		  </div>
		</div>
	  </div>
    )
  }
}

export default Segment;