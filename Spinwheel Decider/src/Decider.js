import React from 'react';
import Spinwheel from './Spinwheel';
import './Decider.css';

class Decider extends React.Component {

  constructor() {
    super();
	
	this.state = {
      choices: [{ choice: "" }],
	  showWheel: false
	}
  }
  
  addChoice = () => {
    this.setState({
      choices: this.state.choices.concat([{ choice: "" }])
    })
  }
  
  removeChoice = idx => () => {
    this.setState({
      choices: this.state.choices.filter((s, sidx) => idx !== sidx)
    })
  }
  
  modifyChoice = idx => evt => {
    const newChoices = this.state.choices.map((choice, sidx) => {
      if (idx !== sidx) {
	    return choice
	  }
      return { ...choice, choice: evt.target.value }
    })
    this.setState({ choices: newChoices })
  }
  
  showHideWheel = () => {
	this.setState({
	  showWheel: !this.state.showWheel
	})
  }
  
  errorMessage = () => {
	return (
	  <div>
	    You must enter at least FOUR choices to Build the Wheel
	  </div>
    )
  }
	
  render() {
    if (!this.state.showWheel) {		
	  return (
        <div>
		  {!(this.state.choices.length > 3) ?
          this.errorMessage :
          null
          }
		
		  {this.state.choices.map((choice, idx) => (
            <div className="Decider-input-container" key={idx}>
              <input type="text" className="Decider-input common-input" placeholder="Enter your choice" value={choice.choice} onChange={this.modifyChoice(idx)} />
              <button type="button" className="button-small common-input" onClick={this.removeChoice(idx)}>
                -
              </button>
            </div>
          ))}
		
		  <button type="button" onClick={this.addChoice} className="button common-input">
            Add Choice
          </button>          
          <br />
		  {this.state.choices.length > 3 ?
            <button type="button" onClick={this.showHideWheel} className="button common-input">
		      Build Wheel
		    </button>	:
            null
          }	  
	    </div>
      )
	}
	else {
	  return (<Spinwheel choices={this.state.choices} hideWheel={this.showHideWheel} />)	
	}
  }
}

export default Decider;