import React from 'react';
import ReactDOM from 'react-dom';
import Segment from './Segment';
import './Spinwheel.css';

class Spinwheel extends React.Component {
	
  constructor() {
    super();
	
	this.state = {
	  rotateDegrees: 0,
	  currentOption: "",
	  segmentLocations: [],
	  checkTimer: false,
	  showChoice: 0
	}
  }
  
  renderSegments = () => {
    let segmentArray = [],
	degrees = 360 / this.props.choices.length,
	skewDegrees = 90 - degrees
	
    for (let i = 0; i < this.props.choices.length; i++) {
      let rotateDegree = i * degrees,
	  segmentId = "segment" + i	  
	  segmentArray.push(<Segment ref={segmentId} choiceText={this.props.choices[i].choice} degree={degrees} 
	    skewDegrees={skewDegrees} rotateDegree={rotateDegree} key={i} />)
    }
    return segmentArray
  }
  
  spinTheWheel = () => {
	let newRotateDegree = this.state.rotateDegrees + (Math.floor(Math.random() * (2200 - 1800 + 1) + 1800)),
	positions = []
	
	for (let i = 0; i < this.props.choices.length; i++) {
	  let segmentId = "segment" + i
	  positions.push(ReactDOM.findDOMNode(this.refs[segmentId]).firstChild)
	}
	
	this.setState({
      rotateDegrees: newRotateDegree,
	  segmentLocations: positions,
	  clickerLocation: ReactDOM.findDOMNode(this.refs["clicker"]).getBoundingClientRect()
    })
	
	if (!this.state.checkTimer) {
	  
	  this.setState({
        checkTimer: true,
		timerCounter: 0
      })
	  
	  let checkCollision = setInterval(() => {	
        for (let i = 0; i < this.props.choices.length; i++) {
          this.checkCollision(this.state.segmentLocations[i])
	    }
		
		this.setState({
		  timerCounter: (this.state.timerCounter + 1)
		})
		
		if (this.state.timerCounter === 100)
		{
	      clearInterval(checkCollision)
		  
		  this.setState({
		    timerCounter: 0,
			checkTimer: false
		  })
		}
      }, 100)	  
	}	
  }	
  
  checkPosition = () => {
	 console.log(ReactDOM.findDOMNode(this.refs["segment0"]).firstChild.getBoundingClientRect()) 
  }
  
  checkCollision = (segment) => {	  
	let segmentRect = segment.getBoundingClientRect(),
	collision = !(
      ((segmentRect.y + segmentRect.height) < (this.state.clickerLocation.y)) ||
      (segmentRect.y > (this.state.clickerLocation.y + this.state.clickerLocation.height)) ||
      ((segmentRect.x + segmentRect.width) < this.state.clickerLocation.x) ||
      (segmentRect.x > (this.state.clickerLocation.x + this.state.clickerLocation.width))
    )
	
	if (collision) {
	  this.setState({
        currentOption: segment.firstChild.firstChild.innerHTML,
		showChoice: 1
      })
	}
  }
	
  render() {		
    return (
	  <div>
        <div className="Spinwheel-container">
	      <div className="Spinwheel-clicker" ref="clicker">
	      </div>
	      <div className="Spinwheel-holder" style={{transform: "translate(-50%, -50%) rotate(" + this.state.rotateDegrees + "deg)"}}>
		    {this.renderSegments()}
		    <div className="Segment-holder">
		    </div>
		  </div>
	    </div>
		<div style={{opacity: this.state.showChoice}}>
		  The Wheel has picked: {this.state.currentOption}
		</div>
	    <button className="button common-input" onClick={this.spinTheWheel}>
		  Spin the Wheel!
	    </button>
		<br />
		<button className="button common-input" onClick={this.checkPosition}>
		  Check Position
	    </button>
		<br />
		<button className="button common-input" onClick={this.props.hideWheel}>
		  Change Choices
		</button>
	  </div>
    )
  }
}

export default Spinwheel;
