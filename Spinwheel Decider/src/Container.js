import React from 'react';
import Decider from './Decider';
import './Container.css';

class Container extends React.Component {

  render() {  
	return (
	  <div className="Container-container">
	    <h3 className="Container-title">
	      Let the Wheel Decide!
	    </h3>
		
		<Decider />
	  </div>
    )
  }
}

export default Container;